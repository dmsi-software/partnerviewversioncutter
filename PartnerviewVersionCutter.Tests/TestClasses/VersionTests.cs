﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace PartnerviewVersionCutter.Tests.TestClasses
{
    [TestClass]
    public class VersionTests
    {
        [TestMethod]
        public void AgilityVersionTest()
        {
            Classes.Version version = new Classes.Version("", "");

            string agilityVersionMatch1 = "5.3.6";
            string agilityVersionMatch2 = "10.10.10";
            string agilityVersionMatch3 = "100.100.100";

            string agilityVersionNotMatch1 = "a.b.c.d";
            string agilityVersionNotMatch2 = "100.2.3.1";
            string agilityVersionNotMatch3 = "10.a.10ai";
            string agilityVersionNotMatch4 = "a.10.10ab";
            string agilityVersionNotMatch5 = "10.10.ab";
            string agilityVersionNotMatch6 = "10.10.abcdefg.";
            string agilityVersionNotMatch7 = "...";
            string agilityVersionNotMatch8 = "";
            string agilityVersionNotMatch9 = "1000.5.5";
            string agilityVersionNotMatch10 = "5.1000.5";
            string agilityVersionNotMatch11 = "5.5.1000";

            Assert.IsTrue(version.RegexMatch(version.AGILITYVERSIONPATTERN, agilityVersionMatch1), $"{agilityVersionMatch1} did not match pattern of {version.AGILITYVERSIONPATTERN}.");
            Assert.IsTrue(version.RegexMatch(version.AGILITYVERSIONPATTERN, agilityVersionMatch2), $"{agilityVersionMatch2} did not match pattern of {version.AGILITYVERSIONPATTERN}.");
            Assert.IsTrue(version.RegexMatch(version.AGILITYVERSIONPATTERN, agilityVersionMatch3), $"{agilityVersionMatch3} did not match pattern of {version.AGILITYVERSIONPATTERN}.");

            Assert.IsFalse(version.RegexMatch(version.AGILITYVERSIONPATTERN, agilityVersionNotMatch1), $"{agilityVersionNotMatch1} matched pattern of {version.AGILITYVERSIONPATTERN}.");
            Assert.IsFalse(version.RegexMatch(version.AGILITYVERSIONPATTERN, agilityVersionNotMatch2), $"{agilityVersionNotMatch2} matched pattern of {version.AGILITYVERSIONPATTERN}.");
            Assert.IsFalse(version.RegexMatch(version.AGILITYVERSIONPATTERN, agilityVersionNotMatch3), $"{agilityVersionNotMatch3} matched pattern of {version.AGILITYVERSIONPATTERN}.");
            Assert.IsFalse(version.RegexMatch(version.AGILITYVERSIONPATTERN, agilityVersionNotMatch4), $"{agilityVersionNotMatch4} matched pattern of {version.AGILITYVERSIONPATTERN}.");
            Assert.IsFalse(version.RegexMatch(version.AGILITYVERSIONPATTERN, agilityVersionNotMatch5), $"{agilityVersionNotMatch5} matched pattern of {version.AGILITYVERSIONPATTERN}.");
            Assert.IsFalse(version.RegexMatch(version.AGILITYVERSIONPATTERN, agilityVersionNotMatch6), $"{agilityVersionNotMatch6} matched pattern of {version.AGILITYVERSIONPATTERN}.");
            Assert.IsFalse(version.RegexMatch(version.AGILITYVERSIONPATTERN, agilityVersionNotMatch7), $"{agilityVersionNotMatch7} matched pattern of {version.AGILITYVERSIONPATTERN}.");
            Assert.IsFalse(version.RegexMatch(version.AGILITYVERSIONPATTERN, agilityVersionNotMatch8), $"{agilityVersionNotMatch8} matched pattern of {version.AGILITYVERSIONPATTERN}.");
            Assert.IsFalse(version.RegexMatch(version.AGILITYVERSIONPATTERN, agilityVersionNotMatch9), $"{agilityVersionNotMatch9} matched pattern of {version.AGILITYVERSIONPATTERN}.");
            Assert.IsFalse(version.RegexMatch(version.AGILITYVERSIONPATTERN, agilityVersionNotMatch10), $"{agilityVersionNotMatch10} matched pattern of {version.AGILITYVERSIONPATTERN}.");
            Assert.IsFalse(version.RegexMatch(version.AGILITYVERSIONPATTERN, agilityVersionNotMatch11), $"{agilityVersionNotMatch11} matched pattern of {version.AGILITYVERSIONPATTERN}.");
        }
        [TestMethod]
        public void PVFixReleaseVersionTest()
        {
            Classes.Version version = new Classes.Version("", "");

            string pvFixReleaseVersionMatch1 = "5.3.6ai";
            string pvFixReleaseVersionMatch2 = "10.10.10aa";
            string pvFixReleaseVersionMatch3 = "100.100.100aaa";

            string pvFixReleaseVersionNotMatch1 = "a.b.c.d";
            string pvFixReleaseVersionNotMatch2 = "100.2.3.1";
            string pvFixReleaseVersionNotMatch3 = "10.a.10ai";
            string pvFixReleaseVersionNotMatch4 = "a.10.10ab";
            string pvFixReleaseVersionNotMatch5 = "10.10.ab";
            string pvFixReleaseVersionNotMatch6 = "10.10.abcdefg.";
            string pvFixReleaseVersionNotMatch7 = "...";
            string pvFixReleaseVersionNotMatch8 = "";
            string pvFixReleaseVersionNotMatch9 = "1000.5.5aa";
            string pvFixReleaseVersionNotMatch10 = "5.1000.5aa";
            string pvFixReleaseVersionNotMatch11 = "5.5.1000aa";
            string pvFixReleaseVersionNotMatch12 = "5.5.100aaaa";
            string pvFixReleaseVersionNotMatch13 = "5.5.5";

            Assert.IsTrue(version.RegexMatch(version.PVFIXRELEASEVERSIONPATTERN, pvFixReleaseVersionMatch1), $"{pvFixReleaseVersionMatch1} did not match pattern of {version.PVFIXRELEASEVERSIONPATTERN}.");
            Assert.IsTrue(version.RegexMatch(version.PVFIXRELEASEVERSIONPATTERN, pvFixReleaseVersionMatch2), $"{pvFixReleaseVersionMatch2} did not match pattern of {version.PVFIXRELEASEVERSIONPATTERN}.");
            Assert.IsTrue(version.RegexMatch(version.PVFIXRELEASEVERSIONPATTERN, pvFixReleaseVersionMatch3), $"{pvFixReleaseVersionMatch3} did not match pattern of {version.PVFIXRELEASEVERSIONPATTERN}.");

            Assert.IsFalse(version.RegexMatch(version.PVFIXRELEASEVERSIONPATTERN, pvFixReleaseVersionNotMatch1), $"{pvFixReleaseVersionNotMatch1} matched pattern of {version.PVFIXRELEASEVERSIONPATTERN}.");
            Assert.IsFalse(version.RegexMatch(version.PVFIXRELEASEVERSIONPATTERN, pvFixReleaseVersionNotMatch2), $"{pvFixReleaseVersionNotMatch2} matched pattern of {version.PVFIXRELEASEVERSIONPATTERN}.");
            Assert.IsFalse(version.RegexMatch(version.PVFIXRELEASEVERSIONPATTERN, pvFixReleaseVersionNotMatch3), $"{pvFixReleaseVersionNotMatch3} matched pattern of {version.PVFIXRELEASEVERSIONPATTERN}.");
            Assert.IsFalse(version.RegexMatch(version.PVFIXRELEASEVERSIONPATTERN, pvFixReleaseVersionNotMatch4), $"{pvFixReleaseVersionNotMatch4} matched pattern of {version.PVFIXRELEASEVERSIONPATTERN}.");
            Assert.IsFalse(version.RegexMatch(version.PVFIXRELEASEVERSIONPATTERN, pvFixReleaseVersionNotMatch5), $"{pvFixReleaseVersionNotMatch5} matched pattern of {version.PVFIXRELEASEVERSIONPATTERN}.");
            Assert.IsFalse(version.RegexMatch(version.PVFIXRELEASEVERSIONPATTERN, pvFixReleaseVersionNotMatch6), $"{pvFixReleaseVersionNotMatch6} matched pattern of {version.PVFIXRELEASEVERSIONPATTERN}.");
            Assert.IsFalse(version.RegexMatch(version.PVFIXRELEASEVERSIONPATTERN, pvFixReleaseVersionNotMatch7), $"{pvFixReleaseVersionNotMatch7} matched pattern of {version.PVFIXRELEASEVERSIONPATTERN}.");
            Assert.IsFalse(version.RegexMatch(version.PVFIXRELEASEVERSIONPATTERN, pvFixReleaseVersionNotMatch8), $"{pvFixReleaseVersionNotMatch8} matched pattern of {version.PVFIXRELEASEVERSIONPATTERN}.");
            Assert.IsFalse(version.RegexMatch(version.PVFIXRELEASEVERSIONPATTERN, pvFixReleaseVersionNotMatch9), $"{pvFixReleaseVersionNotMatch9} matched pattern of {version.PVFIXRELEASEVERSIONPATTERN}.");
            Assert.IsFalse(version.RegexMatch(version.PVFIXRELEASEVERSIONPATTERN, pvFixReleaseVersionNotMatch10), $"{pvFixReleaseVersionNotMatch10} matched pattern of {version.PVFIXRELEASEVERSIONPATTERN}.");
            Assert.IsFalse(version.RegexMatch(version.PVFIXRELEASEVERSIONPATTERN, pvFixReleaseVersionNotMatch11), $"{pvFixReleaseVersionNotMatch11} matched pattern of {version.PVFIXRELEASEVERSIONPATTERN}.");
            Assert.IsFalse(version.RegexMatch(version.PVFIXRELEASEVERSIONPATTERN, pvFixReleaseVersionNotMatch12), $"{pvFixReleaseVersionNotMatch12} matched pattern of {version.PVFIXRELEASEVERSIONPATTERN}.");
            Assert.IsFalse(version.RegexMatch(version.PVFIXRELEASEVERSIONPATTERN, pvFixReleaseVersionNotMatch13), $"{pvFixReleaseVersionNotMatch13} matched pattern of {version.PVFIXRELEASEVERSIONPATTERN}.");
        }

        [TestMethod]
        public void ValidVersionFormatTest()
        {
            string agilityVersionMatch1 = "5.3.6";
            string agilityVersionNotMatch1 = "a.b.c.d";
            string pvFixReleaseVersionMatch1 = "5.3.6ai";
            string pvFixReleaseVersionNotMatch1 = "a.b.c.d";

            Classes.Version version1 = new Classes.Version(pvFixReleaseVersionMatch1, agilityVersionMatch1);
            Classes.Version version2 = new Classes.Version(pvFixReleaseVersionMatch1, agilityVersionNotMatch1);
            Classes.Version version3 = new Classes.Version(pvFixReleaseVersionNotMatch1, agilityVersionMatch1);
            Classes.Version version4 = new Classes.Version(pvFixReleaseVersionNotMatch1, pvFixReleaseVersionNotMatch1);

            Assert.IsTrue(version1.ValidVersionFormat(Classes.Version.VersionNumberToValidate.None));
            Assert.IsTrue(version1.ValidVersionFormat(Classes.Version.VersionNumberToValidate.FixRelease));
            Assert.IsTrue(version1.ValidVersionFormat(Classes.Version.VersionNumberToValidate.Agility));
            Assert.IsTrue(version1.ValidVersionFormat(Classes.Version.VersionNumberToValidate.Both));

            Assert.IsTrue(version2.ValidVersionFormat(Classes.Version.VersionNumberToValidate.None));
            Assert.IsTrue(version2.ValidVersionFormat(Classes.Version.VersionNumberToValidate.FixRelease));
            Assert.IsFalse(version2.ValidVersionFormat(Classes.Version.VersionNumberToValidate.Agility));
            Assert.IsFalse(version2.ValidVersionFormat(Classes.Version.VersionNumberToValidate.Both));

            Assert.IsTrue(version3.ValidVersionFormat(Classes.Version.VersionNumberToValidate.None));
            Assert.IsFalse(version3.ValidVersionFormat(Classes.Version.VersionNumberToValidate.FixRelease));
            Assert.IsTrue(version3.ValidVersionFormat(Classes.Version.VersionNumberToValidate.Agility));
            Assert.IsFalse(version3.ValidVersionFormat(Classes.Version.VersionNumberToValidate.Both));

            Assert.IsTrue(version4.ValidVersionFormat(Classes.Version.VersionNumberToValidate.None));
            Assert.IsFalse(version4.ValidVersionFormat(Classes.Version.VersionNumberToValidate.FixRelease));
            Assert.IsFalse(version4.ValidVersionFormat(Classes.Version.VersionNumberToValidate.Agility));
            Assert.IsFalse(version4.ValidVersionFormat(Classes.Version.VersionNumberToValidate.Both));
        }
        [TestMethod]
        public void IncrementVersionNumberTest()
        {
            Classes.Version version = new Classes.Version("", "");

            string versionToIncrement1 = "5.3.6.1";
            string versionToIncrement2 = "1.1.1.300";
            string versionToIncrement3 = "100.100.100.99";
            string versionToIncrement4 = "1000.1000.1000.10000";
            string versionToIncrement5 = "5.3.6.0";

            string versionToIncrement1Expected = "5.3.6.2";
            string versionToIncrement2Expected = "1.1.1.301";
            string versionToIncrement3Expected = "100.100.100.100";
            string versionToIncrement4Expected = "1000.1000.1000.10001";
            string versionToIncrement5Expected = "5.3.6.1";

            Assert.AreEqual(version.IncrementVersionNumber(versionToIncrement1), versionToIncrement1Expected);
            Assert.AreEqual(version.IncrementVersionNumber(versionToIncrement2), versionToIncrement2Expected);
            Assert.AreEqual(version.IncrementVersionNumber(versionToIncrement3), versionToIncrement3Expected);
            Assert.AreEqual(version.IncrementVersionNumber(versionToIncrement4), versionToIncrement4Expected);
            Assert.AreEqual(version.IncrementVersionNumber(versionToIncrement5), versionToIncrement5Expected);
        }
    }
}
