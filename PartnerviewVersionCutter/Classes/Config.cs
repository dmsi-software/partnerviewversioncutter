﻿using System;
using System.Xml;

namespace PartnerviewVersionCutter.Classes
{
    interface IConfig { }
    public class Config : IConfig
    {
        #region "Properties"
        private string _cPath = "";
        private string _cErrorMessage = "";
        private XmlDocument _xDoc = new XmlDocument();
        #endregion
        #region "Constructors"
        public Config(string pcDirectory)
        {
            _cPath = pcDirectory + "\\web.config";
        }
        #endregion
        #region "Public Methods"
        public bool LoadFileFromNetwork()
        {
            try
            {
                _xDoc.Load(_cPath);
                System.Xml.XmlElement Root = _xDoc.DocumentElement;

                return true;
            }
            catch (System.IO.FileNotFoundException)
            {
                _cErrorMessage += $"No configuration file found at path {_cPath}.";
            }
            catch (Exception ex)
            {
                _cErrorMessage += ex.InnerException.Message;
            }
            return false;
        }
        public bool LoadFileFromString(string cXML)
        {
            try
            {
                _xDoc.LoadXml(cXML);
                System.Xml.XmlElement Root = _xDoc.DocumentElement;

                return true;
            }
            catch (Exception ex)
            {
                _cErrorMessage += ex.InnerException.Message;
            }
            return false;
        }
        public bool SaveFileToNetwork()
        {
            try
            {
                if (_cPath == "")
                {
                    throw new Exception("Trying to save file to network without a path specified!");
                }

                _xDoc.Save(_cPath);

                return true;
            }
            catch (Exception e)
            {
                _cErrorMessage += e.InnerException.Message;
            }

            return false;
        }
        #region "XML Nodes"
        public bool SetAttributeValue(string pcTag, string pcAttribute, string pcValue)
        {
            if (!ValidTag(pcTag)) { return false; }
            if (!ValidAttribute(pcTag, pcAttribute)) { return false; }

            XmlNode node = _xDoc.GetElementsByTagName(pcTag).Item(0);

            node.Attributes[pcAttribute].Value = pcValue;

            return true;
        }
        public bool AddOrSetAttributeValue(string pcTag, string pcAttribute, string pcValue)
        {
            if (!ValidTag(pcTag)) { return false; }
            if (ValidAttribute(pcTag, pcAttribute))
            {
                return SetAttributeValue(pcTag, pcAttribute, pcValue);
            }
            else
            {
                XmlNode node = _xDoc.GetElementsByTagName(pcTag).Item(0);
                XmlAttribute attr = _xDoc.CreateAttribute(pcAttribute);
                attr.Value = pcValue;

                node.Attributes.Append(attr);
            }

            return true;
        }
        #endregion
        #region "App Settings"
        public bool SetAppSetting(string pcKey, string pcValue)
        {
            try
            {
                System.Xml.XmlElement Root = _xDoc.DocumentElement;
                string xpath = $"appSettings/add[@key='{pcKey}']";
                System.Xml.XmlNode node = Root.SelectSingleNode(xpath);
                if (node != null)
                {
                    node.Attributes["value"].Value = pcValue;
                }
                else
                {
                    throw new Exception($"Cannot find AppSetting value with key {pcKey}");
                }
            }
            catch (System.Exception ex)
            {
                string message = "Exception: SetAppSetting()";
                message += System.Environment.NewLine + ex.ToString();
            }
            return true;
        }

        public string GetAppSetting(string pcKey)
        {
            XmlElement Root = _xDoc.DocumentElement;
            string xpath = $"appSettings/add[@key='{pcKey}']";
            XmlNode node = Root.SelectSingleNode(xpath);
            if (node != null)
            {
                return node.Attributes["value"].Value;
            }

            throw new Exception($"Could not find AppSetting value with key {pcKey}!");
        }
        #endregion
        public string ErrorMessage()
        {
            return _cErrorMessage;
        }
        #endregion
        #region "Private Methods"
        #region "Validation"
        private bool ValidTag(string pcTag)
        {
            bool lValidTag = true;
            int nNodeCount = _xDoc.GetElementsByTagName(pcTag).Count;

            if (nNodeCount == 0)
            {
                _cErrorMessage += $"No nodes found in the config file with tag name of {pcTag}." + Environment.NewLine;
                lValidTag = false;
            }
            else if (nNodeCount > 1)
            {
                _cErrorMessage += $"There are too many nodes with the name {pcTag} in the config file.  Re-evaluate your code, dude!" + Environment.NewLine;
                lValidTag = false;
            }

            return lValidTag;
        }
        private bool ValidAttribute(string pcTag, string pcAttribute)
        {
            XmlNode node = _xDoc.GetElementsByTagName(pcTag).Item(0);

            if (node.Attributes[pcAttribute] != null)
            {
                return true;
            }

            return false;
        }
        #endregion
        #endregion
    }
}
