﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PartnerviewVersionCutter.Classes
{
    interface IVersion { }
    public class Version : IVersion
    {
        #region "Properties"
        public enum VersionNumberToValidate
        {
            None = 0,
            FixRelease = 1,
            Agility = 2,
            Both = 3
        }
        public readonly string _AGILITYVERSIONPATTERN = @"^(\d{1,3})\.(\d{1,3})\.(\d{1,3})$";
        public readonly string _PVFIXRELEASEVERSIONPATTERN = @"^(\d{1,3})\.(\d{1,3})\.(\d{1,3})([a-z]{1,3})$";
        private readonly string _cFixReleaseVersion;
        private readonly string _cAgilityVersion;
        private string _cErrorMessage = "";
        #endregion
        #region "Constructors"
        public Version(string pcFixReleaseVersion, string pcAgilityVersion)
        {
            _cFixReleaseVersion = pcFixReleaseVersion;
            _cAgilityVersion = pcAgilityVersion;
        }
        #endregion
        #region "Public Methods"
        public bool ValidVersionFormat(VersionNumberToValidate pVersion)
        {
            switch (pVersion)
            {
                case VersionNumberToValidate.None:
                    return true;
                case VersionNumberToValidate.FixRelease:
                    return ValidFixReleaseVersionFormat();
                case VersionNumberToValidate.Agility:
                    return ValidAgilityVersionFormat();
                case VersionNumberToValidate.Both:
                    return ValidAgilityVersionFormat() && ValidFixReleaseVersionFormat();
                default:
                    throw new NotImplementedException($"Version enumeration of {pVersion} passed to Version.ValidVersionFormat method.");
            }
        }
        public bool RegexMatch(string pcRegex, string pcVersionNumber)
        {
            Regex regex = new Regex(pcRegex);

            return regex.IsMatch(pcVersionNumber);
        }
        public string AgilityVersion()
        {
            return _cAgilityVersion;
        }
        public string AssumedAgilityVersion()
        {
            return Regex.Replace(_cFixReleaseVersion, "[^0-9.]", "");
        }
        public string FixReleaseVersionDotFormat()
        {
            return _cFixReleaseVersion;
        }
        public string FixReleaseVersionUnderscoreFormat()
        {
            return _cFixReleaseVersion.Replace(".","_");
        }
        public string FixReleaseExecutableName()
        {
            return "pv_" + FixReleaseVersionUnderscoreFormat().Replace("v", "") + ".exe";
        }
        public string IncrementVersionNumber(string pcCurrentVersionNumber)
        {
            string[] cCurrentVersionParts = pcCurrentVersionNumber.Split('.');

            return cCurrentVersionParts[0] + '.' +
                   cCurrentVersionParts[1] + '.' +
                   cCurrentVersionParts[2] + '.' +
                   (Convert.ToInt32(cCurrentVersionParts[3]) + 1).ToString();
        }
        public string ErrorMessage()
        {
            return _cErrorMessage;
        }
        #endregion
        #region "Private Methods"
        private bool ValidAgilityVersionFormat()
        {
            bool valid = RegexMatch(_AGILITYVERSIONPATTERN, _cAgilityVersion);

            if (!valid)
            {
                _cErrorMessage += $"Not a valid Agility version format.  Please enter a valid version in the format of {this._AGILITYVERSIONPATTERN}." +
                                  Environment.NewLine;
            }

            return valid;
        }
        private bool ValidFixReleaseVersionFormat()
        {
            bool valid = RegexMatch(_PVFIXRELEASEVERSIONPATTERN, _cFixReleaseVersion);

            if (!valid)
            {
                _cErrorMessage += $"Not a valid fix release version format.  Please enter a valid version in the format of {this._PVFIXRELEASEVERSIONPATTERN}." +
                                 Environment.NewLine;
            }

            return valid;
        }
        #endregion
    }
}
