﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using SharpSvn;
using System.Collections.Generic;
using Microsoft.Build.Evaluation;
using Microsoft.Build.Execution;
using System.Text.RegularExpressions;
using System.Configuration;

namespace PartnerviewVersionCutter
{
    public partial class frmMain : Form
    {
        private string _VisualStudioDirectory = "";
        private string _MainAgilityVersionNumber = "";

        private string _WebSiteDirectory = "";
        private string _ProjectDirectory = "";
        private string _ConfigFileDirectory = "";

        private readonly string _MakePVZipFixSourceDirectory = "C:\\Scripts";
        private readonly string _MakePVZipFileName = "makePVfixzip.vbs";

        private string _MakePVZipFixDestinationDirectory = "";
        private string _SolutionFileLocation = "";

        public frmMain()
        {
            InitializeComponent();

            /*Doing this until I have time to work on the full PV version cut tab*/
            tabVersionType.SelectedTab = tabVersionType.TabPages["tabFixRelease"];
            this.ActiveControl = txtFRPVVersionNumber;
            btnFullCut.Enabled = false;
            txtFullPVVersionNumber.Enabled = false;
            txtVSProjectDirectory.Text = ConfigurationManager.AppSettings["VisualStudioProjectDirectory"].ToString().Trim();

            //if (Debugger.IsAttached)
            //{
            //    txtFRPVVersionNumber.Text = "99.99.99aa";
            //    txtFRAgilityVersionNumber.Text = "99.99.99";
            //    PerformFixRelease();
            //}
        }
        
        private void btnFRCut_Click(object sender, EventArgs e)
        {
            PerformFixRelease();
        }

        private void PerformFixRelease()
        {
            lblFRError.Text = "";

            _MainAgilityVersionNumber = Regex.Replace(txtFRPVVersionNumber.Text, "[^0-9.]", "");

            //Validate PV and Agility version number via version class using regex
            Classes.Version version = new Classes.Version(txtFRPVVersionNumber.Text, _MainAgilityVersionNumber);
            if (!version.ValidVersionFormat(Classes.Version.VersionNumberToValidate.Both))
            {
                lblFRError.Text += version.ErrorMessage();
                return;
            }

            _ProjectDirectory = $"C:\\Projects\\PV {version.AgilityVersion()} Branch";
            _WebSiteDirectory = $"C:\\Web Sites\\PV v{version.AgilityVersion()}";
            _SolutionFileLocation = $"{ _ProjectDirectory}\\PV {version.AgilityVersion()} Branch.sln";

            //Check for existence of correct project and website folders
            if (!Directory.Exists(_ProjectDirectory))
            {
                lblFRError.Text += $"The {_ProjectDirectory} folder do not exist for version {version.AgilityVersion()} yet." + Environment.NewLine;
                //TODO: Checkout and build?  
            }
            if (!Directory.Exists(_WebSiteDirectory))
            {
                lblFRError.Text += $"The {_WebSiteDirectory} folder do not exist for version {version.AgilityVersion()} yet." + Environment.NewLine;
            }

            //Check for existence of correct solution file
            if (!File.Exists(_SolutionFileLocation))
            {
                lblFRError.Text += $"The {_SolutionFileLocation} solution file do not exist." + Environment.NewLine;
            }

            if (lblFRError.Text != "") return;


            ////SVN Update correct project and website folders
            //if (!Debugger.IsAttached)
            //{
            //    using (SvnClient client = new SvnClient())
            //    {
            //        SvnUpdateResult result;

            //        client.Authentication.DefaultCredentials = System.Net.CredentialCache.DefaultCredentials;
            //        client.Update(_ProjectDirectory, out result);
            //        client.Update(_WebSiteDirectory, out result);
            //    }
            //}

            //if (Debugger.IsAttached)
            //{
            //    _ConfigFileDirectory = "C:\\Web Sites\\PV v99.99.99";
            //}
            //else
            //{
            //    _ConfigFileDirectory = _WebSiteDirectory;
            //}

            //Classes.Config modifiableConfigFile = new Classes.Config(_ConfigFileDirectory);

            //modifiableConfigFile.LoadFileFromNetwork();
            //modifiableConfigFile.SetAppSetting("agilityversion", version.FixReleaseVersionDotFormat());
            //modifiableConfigFile.SetAppSetting("internalversion", version.IncrementVersionNumber(modifiableConfigFile.GetAppSetting("internalversion")));
            //modifiableConfigFile.SetAppSetting("version", version.FixReleaseVersionDotFormat());
            //modifiableConfigFile.SaveFileToNetwork();

            ////Commit changes with the specified log message
            //if (!Debugger.IsAttached)
            //{
            //    using (SvnClient client = new SvnClient())
            //    {
            //        SvnCommitArgs ca = new SvnCommitArgs();

            //        client.Authentication.DefaultCredentials = System.Net.CredentialCache.DefaultCredentials;

            //        ca.LogMessage = $"Bumping fix release version to {version.FixReleaseVersionDotFormat()}.";

            //        client.Commit(_ProjectDirectory, ca);
            //        client.Commit(_WebSiteDirectory, ca);
            //    }
            //}

            ////MSBuild           
            //Dictionary<string, string> GlobalProperties = new Dictionary<string, string>();

            //GlobalProperties.Add("Configuration", "Release");
            //GlobalProperties.Add("Platform", "Any CPU");

            //BuildRequestData buildRequest = new BuildRequestData(_SolutionFileLocation, GlobalProperties, null, new string[] { "Build" }, null);
            //BuildResult buildResult = BuildManager.DefaultBuildManager.Build(new BuildParameters(new ProjectCollection()), buildRequest);


            //Alter PrecompiledWeb web config
            DirectoryInfo precompWebDir = new DirectoryInfo(_ProjectDirectory + "\\PrecompiledWeb");
            _MakePVZipFixDestinationDirectory = precompWebDir.GetDirectories().First().FullName;

            Classes.Config configFile = new Classes.Config(_MakePVZipFixDestinationDirectory);

            configFile.LoadFileFromNetwork();
            configFile.SetAppSetting("AgilityLibraryConnection", "");
            configFile.SetAppSetting("TcpIp", "");
            configFile.SetAppSetting("SSL", "false");
            configFile.SetAppSetting("ForceHTTPS", "false");
            configFile.SetAppSetting("StartProductSearchFirst", "false");
            configFile.SetAppSetting("ProductSearchUserName", "");
            configFile.SetAppSetting("ProductSearchPassword", "");
            configFile.SetAppSetting("com.dmsi.agility.Session", "");
            configFile.SetAppSetting("com.dmsi.agility.Inventory", "");
            configFile.SetAttributeValue("compilation", "debug", "false");
            configFile.SetAttributeValue("customErrors", "mode", "RemoteOnly");
            configFile.AddOrSetAttributeValue("customErrors", "defaultRedirect", "GenericError.htm");

            if (configFile.ErrorMessage() != "")
            {
                lblFRError.Text += configFile.ErrorMessage();
                return;
            }

            configFile.SaveFileToNetwork();

            ////Copy mkPVFixzip.vbs from C:\Scripts to C:\Projects\<correct folder>\precompiledweb\child folder?\
            //File.Copy(_MakePVZipFixSourceDirectory + "\\" + _MakePVZipFileName,
            //          _MakePVZipFixDestinationDirectory + "\\" + _MakePVZipFileName);

            ////TODO: Test this code once MSBuild performed
            ////Command line call mkPVFixZip.vbs in build directory C:\Projects\<correct folder>\precompiledweb\child folder?\
            ////using format <Major>_<Minor>_<Release><FixRelease>
            //Process.Start("CMD.exe", _MakePVZipFixDestinationDirectory + "\\" +
            //                         _MakePVZipFileName + " " + version.FixReleaseVersionUnderscoreFormat());

            ////TODO: Alter makePVFix to automatically do whatever the popup prompts do for OK, No, Yes sequence

            ////TODO: Delete .exe and makepvfix.vbs files from precompiled web directory.

            //try
            //{
            //    File.Delete(_MakePVZipFixDestinationDirectory + "\\" + version.FixReleaseExecutableName());
            //    File.Delete(_MakePVZipFixDestinationDirectory + "\\" + _MakePVZipFileName);
            //}
            //catch (Exception e)
            //{
            //    lblFRError.Text += e.Message + Environment.NewLine;
            //    return;
            //}

            MessageBox.Show($"Finished bundling fix release {version.FixReleaseVersionDotFormat()} as {version.FixReleaseExecutableName()}." +
                             Environment.NewLine +
                             "Please verify the package made it to releases.dmsi.com.");
        }

        private void txtFRVersionNumber_Leave(object sender, EventArgs e)
        {
            lblFRError.Text = "";

            if (txtFRPVVersionNumber.Text.Trim() == "") { return; }

            Classes.Version version = new Classes.Version(txtFRPVVersionNumber.Text, "");
            if (!version.ValidVersionFormat(Classes.Version.VersionNumberToValidate.FixRelease))
            {
                lblFRError.Text = version.ErrorMessage();
                txtFRPVVersionNumber.Focus();
                txtFRPVVersionNumber.SelectAll();
            }
        }

        private void frmMain_Load(object sender, EventArgs e)
        {

        }
    }
}
