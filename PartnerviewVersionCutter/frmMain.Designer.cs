﻿namespace PartnerviewVersionCutter
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.tabVersionType = new System.Windows.Forms.TabControl();
            this.tabFull = new System.Windows.Forms.TabPage();
            this.txtFullPVVersionNumber = new System.Windows.Forms.TextBox();
            this.lblFullVersionNumber = new System.Windows.Forms.Label();
            this.btnFullCut = new System.Windows.Forms.Button();
            this.tabFixRelease = new System.Windows.Forms.TabPage();
            this.lblFRError = new System.Windows.Forms.Label();
            this.txtFRPVVersionNumber = new System.Windows.Forms.TextBox();
            this.lblFRVersionNumber = new System.Windows.Forms.Label();
            this.btnFRCut = new System.Windows.Forms.Button();
            this.txtVSProjectDirectory = new System.Windows.Forms.TextBox();
            this.lblVSProjectDirectory = new System.Windows.Forms.Label();
            this.tabVersionType.SuspendLayout();
            this.tabFull.SuspendLayout();
            this.tabFixRelease.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabVersionType
            // 
            this.tabVersionType.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabVersionType.Controls.Add(this.tabFull);
            this.tabVersionType.Controls.Add(this.tabFixRelease);
            this.tabVersionType.Location = new System.Drawing.Point(0, 3);
            this.tabVersionType.Name = "tabVersionType";
            this.tabVersionType.SelectedIndex = 0;
            this.tabVersionType.Size = new System.Drawing.Size(502, 292);
            this.tabVersionType.TabIndex = 0;
            // 
            // tabFull
            // 
            this.tabFull.Controls.Add(this.txtFullPVVersionNumber);
            this.tabFull.Controls.Add(this.lblFullVersionNumber);
            this.tabFull.Controls.Add(this.btnFullCut);
            this.tabFull.Location = new System.Drawing.Point(4, 22);
            this.tabFull.Name = "tabFull";
            this.tabFull.Padding = new System.Windows.Forms.Padding(3);
            this.tabFull.Size = new System.Drawing.Size(390, 266);
            this.tabFull.TabIndex = 0;
            this.tabFull.Text = "Full";
            this.tabFull.UseVisualStyleBackColor = true;
            // 
            // txtFullPVVersionNumber
            // 
            this.txtFullPVVersionNumber.Location = new System.Drawing.Point(96, 15);
            this.txtFullPVVersionNumber.Name = "txtFullPVVersionNumber";
            this.txtFullPVVersionNumber.Size = new System.Drawing.Size(185, 20);
            this.txtFullPVVersionNumber.TabIndex = 1;
            // 
            // lblFullVersionNumber
            // 
            this.lblFullVersionNumber.AutoSize = true;
            this.lblFullVersionNumber.Location = new System.Drawing.Point(8, 18);
            this.lblFullVersionNumber.Name = "lblFullVersionNumber";
            this.lblFullVersionNumber.Size = new System.Drawing.Size(82, 13);
            this.lblFullVersionNumber.TabIndex = 1;
            this.lblFullVersionNumber.Text = "Version Number";
            // 
            // btnFullCut
            // 
            this.btnFullCut.Location = new System.Drawing.Point(8, 237);
            this.btnFullCut.Name = "btnFullCut";
            this.btnFullCut.Size = new System.Drawing.Size(75, 23);
            this.btnFullCut.TabIndex = 3;
            this.btnFullCut.Text = "Cut";
            this.btnFullCut.UseVisualStyleBackColor = true;
            // 
            // tabFixRelease
            // 
            this.tabFixRelease.Controls.Add(this.txtVSProjectDirectory);
            this.tabFixRelease.Controls.Add(this.lblVSProjectDirectory);
            this.tabFixRelease.Controls.Add(this.lblFRError);
            this.tabFixRelease.Controls.Add(this.txtFRPVVersionNumber);
            this.tabFixRelease.Controls.Add(this.lblFRVersionNumber);
            this.tabFixRelease.Controls.Add(this.btnFRCut);
            this.tabFixRelease.Location = new System.Drawing.Point(4, 22);
            this.tabFixRelease.Name = "tabFixRelease";
            this.tabFixRelease.Padding = new System.Windows.Forms.Padding(3);
            this.tabFixRelease.Size = new System.Drawing.Size(494, 266);
            this.tabFixRelease.TabIndex = 1;
            this.tabFixRelease.Text = "Fix Release";
            this.tabFixRelease.UseVisualStyleBackColor = true;
            // 
            // lblFRError
            // 
            this.lblFRError.AutoSize = true;
            this.lblFRError.ForeColor = System.Drawing.Color.Red;
            this.lblFRError.Location = new System.Drawing.Point(11, 79);
            this.lblFRError.MaximumSize = new System.Drawing.Size(385, 0);
            this.lblFRError.Name = "lblFRError";
            this.lblFRError.Size = new System.Drawing.Size(0, 13);
            this.lblFRError.TabIndex = 5;
            // 
            // txtFRPVVersionNumber
            // 
            this.txtFRPVVersionNumber.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFRPVVersionNumber.Location = new System.Drawing.Point(157, 15);
            this.txtFRPVVersionNumber.Name = "txtFRPVVersionNumber";
            this.txtFRPVVersionNumber.Size = new System.Drawing.Size(331, 20);
            this.txtFRPVVersionNumber.TabIndex = 1;
            this.txtFRPVVersionNumber.Leave += new System.EventHandler(this.txtFRVersionNumber_Leave);
            // 
            // lblFRVersionNumber
            // 
            this.lblFRVersionNumber.AutoSize = true;
            this.lblFRVersionNumber.Location = new System.Drawing.Point(69, 18);
            this.lblFRVersionNumber.Name = "lblFRVersionNumber";
            this.lblFRVersionNumber.Size = new System.Drawing.Size(82, 13);
            this.lblFRVersionNumber.TabIndex = 3;
            this.lblFRVersionNumber.Text = "Version Number";
            this.lblFRVersionNumber.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // btnFRCut
            // 
            this.btnFRCut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnFRCut.Location = new System.Drawing.Point(8, 237);
            this.btnFRCut.Name = "btnFRCut";
            this.btnFRCut.Size = new System.Drawing.Size(75, 23);
            this.btnFRCut.TabIndex = 3;
            this.btnFRCut.Text = "Cut";
            this.btnFRCut.UseVisualStyleBackColor = true;
            this.btnFRCut.Click += new System.EventHandler(this.btnFRCut_Click);
            // 
            // txtVSProjectDirectory
            // 
            this.txtVSProjectDirectory.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtVSProjectDirectory.Location = new System.Drawing.Point(157, 37);
            this.txtVSProjectDirectory.Name = "txtVSProjectDirectory";
            this.txtVSProjectDirectory.Size = new System.Drawing.Size(331, 20);
            this.txtVSProjectDirectory.TabIndex = 2;
            // 
            // lblVSProjectDirectory
            // 
            this.lblVSProjectDirectory.AutoSize = true;
            this.lblVSProjectDirectory.Location = new System.Drawing.Point(8, 40);
            this.lblVSProjectDirectory.Name = "lblVSProjectDirectory";
            this.lblVSProjectDirectory.Size = new System.Drawing.Size(149, 13);
            this.lblVSProjectDirectory.TabIndex = 6;
            this.lblVSProjectDirectory.Text = "Visual Studio Project Directory";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(504, 297);
            this.Controls.Add(this.tabVersionType);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMain";
            this.Text = "Partnerview Version Cutter";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.tabVersionType.ResumeLayout(false);
            this.tabFull.ResumeLayout(false);
            this.tabFull.PerformLayout();
            this.tabFixRelease.ResumeLayout(false);
            this.tabFixRelease.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabVersionType;
        private System.Windows.Forms.TabPage tabFull;
        private System.Windows.Forms.TextBox txtFullPVVersionNumber;
        private System.Windows.Forms.Label lblFullVersionNumber;
        private System.Windows.Forms.Button btnFullCut;
        private System.Windows.Forms.TabPage tabFixRelease;
        private System.Windows.Forms.Button btnFRCut;
        private System.Windows.Forms.Label lblFRVersionNumber;
        private System.Windows.Forms.TextBox txtFRPVVersionNumber;
        private System.Windows.Forms.Label lblFRError;
        private System.Windows.Forms.TextBox txtVSProjectDirectory;
        private System.Windows.Forms.Label lblVSProjectDirectory;
    }
}

